syntax enable
set ruler

set incsearch
set ignorecase
set hlsearch

set expandtab

set tabstop=2
set shiftwidth=2

set wrap


" Make the current file's directory to be default browse directory when
" opening new file
set browsedir=buffer

filetype off " required!

let mapleader=" "

autocmd BufWinLeave *.* mkview
autocmd BufWinEnter *.* silent loadview 

" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" let Vundle manage Vundle
" required! 
Bundle 'gmarik/vundle'
Bundle 'cakebaker/scss-syntax.vim'
Bundle 'kana/vim-arpeggio'
set runtimepath^=~/.vim/bundle/ctrlp.vim

function! SetupChord()
    " Arpeggio inoremap hjkl print()<Left>
    " Arpeggio inoremap sdf function

    " LUA
    " Arpeggio inoremap asfd function()<Enter>end<ESC>O
    " Arpeggio inoremap wert timer.add(1000, function()<Enter>end)<ESC>O
    " Arpeggio inoremap ert transition.to(,{<Enter>})<ESC>O
    Arpeggio inoremap jk <ESC>
    Arpeggio xnoremap jk <ESC>
    Arpeggio nnoremap jk :noh<CR><CR>
    " Arpeggio nnoremap zxc ^i--<ESC>
    " Arpeggio nnoremap zx ^xx
endfunction

autocmd VimEnter * call SetupChord()

"Drupal Highlight
au BufNewFile,BufRead *.php,*.php\d,*.module,*.inc,*.install setf php 

filetype plugin indent on     " required!
"
" Brief help
" :BundleList          - list configured bundles
" :BundleInstall(!)    - install(update) bundles
" :BundleSearch(!) foo - search(or refresh cache first) for foo
" :BundleClean(!)      - confirm(or auto-approve) removal of unused bundles
"
" see :h vundle for more details or wiki for FAQ
" NOTE: comments after Bundle command are not allowed..
"

